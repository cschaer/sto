// event listeners

document.getElementById('inspo').addEventListener('click', async()=>{
    await getAndSave('inspo')
})
document.getElementById('garten').addEventListener('click', async()=>{
    await getAndSave('garten')
})
document.getElementById('essen').addEventListener('click', async()=>{
    await getAndSave('essen')
})
document.getElementById('server-plex').addEventListener('click', async()=>{
    await getAndSave('server-plex')
})
document.getElementById('travel').addEventListener('click', async()=>{
    await getAndSave('travel')
})
document.getElementById('hydro').addEventListener('click', async()=>{
    await getAndSave('hydro')
})

let getAndSave = async ( category ) => {
    chrome.tabs.query({ active: true, currentWindow: true }, (tabs) =>{
        var currentUrl = tabs[0].url
        var tabId = tabs[0].id
        var pageTitle = tabs[0].title
        document.getElementById("test").innerHTML=currentUrl
        sendToApi(category,currentUrl, pageTitle, tabId)
    })
}

let sendToApi = (category, url, title, tab) => {
    console.log(url)
    fetch(`http://localhost:3000/?category=${category}&url=${url}&title=${title}`)
        .then((res)=>{
            if (!res.ok)
                document.getElementById("test").innerHTML = "failed"
            return res.text()
        }).then((res)=>{
            document.getElementById("test").innerHTML = res
            chrome.tabs.remove(tab)
        })
}
