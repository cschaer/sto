import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { TabpageComponent } from './tabpage/tabpage.component';


const routes: Routes = [
  // { path: '', redirectTo: '/dropdown', pathMatch: 'full'},
  { path: '', component: AppComponent},
  { path: 'dropdown', component: DropdownComponent },
  { path: 'tabpage', component: TabpageComponent },
]


@NgModule({
  declarations: [],
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
