import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TabpageComponent } from './tabpage/tabpage.component';
import { AppRoutingModule } from './app-routing.module';
import { DropdownComponent } from './dropdown/dropdown.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    TabpageComponent,
    DropdownComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
