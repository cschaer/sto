/// <reference types="chrome"/>
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
class StorageService {

  constructor() {
    //
          // let date = Date()
          // this.clearStorage()
          // this.storeData(
          //   [
          //     new Tab(`streamdeck-ui`, `https://timothycrosley.github.io/streamdeck-ui/`, 'inspo',`https://cdn.sstatic.net/Sites/stackoverflow/Img/favicon.ico?v=ec617d715196`, date),
          //     new Tab(`Beach driving? : subaruoutback`, `https://www.reddit.com/r/subaruoutback/comments/hp1yiv/beach_driving/`, 'inspo'),
          //   ],
          //   [ new Category(`inspo`, `#5E3436`, true),new Category(`garten`, `#2F443A`)]
          // )
    //
  }

  // methods ... save link, delete link, get list, create/update category, set cat visible ... save/retrieve list from file/storage?

  saveTab = async (title:string|any, url:string|any, categoryName:string, faviconUrl?:string|undefined, dateAdded?:string):Promise<boolean> => {
    let tabList!:Tab[]
    // console.log('inSaveTab')
    return new Promise(async (res,rej)=>{
      this.getFromStorage().then(async x=>{
        tabList = [...x[0], new Tab(title, url, categoryName, faviconUrl, dateAdded)]
        await this.storeData(tabList, x[1]).then(x=>res(true))
      }).catch(x=>{
        console.debug('failed to get from storage', x)
        // TODO: logic needed here for new categories or no existing tabs
        // tabList = [new Tab(title, url, categoryName, faviconUrl, dateAdded)]
        // await
      })

    })
  }
      // let matchingCategoryList = this.categoryList.filter(x=>x.title===categoryName)
      // let category = matchingCategoryList.length === 1 ? matchingCategoryList[0] : new Category(categoryName)

  storeData = async (tabList:Array<Tab> ,catList:Array<Category>) => {
    chrome.storage.sync.set({stoTabs: [tabList, catList]}, ()=>{
      console.debug('synced tabs and categories')
    })
  }

  getFromStorage = async():Promise<[Tab[], Category[]]> => {
    // console.log(`getFromStorage`)
    let tabList!:Tab[], catList!:Category[]
    return new Promise(async (res,rej)=>{
      try {
        console.log('ooh, we tryin')
        chrome.storage.sync.get(['stoTabs'], (data) => {
          console.log(data)
          console.log(chrome.runtime.lastError)
          tabList = data[`stoTabs`][0].map((x: any) => {
            console.log(chrome.runtime.lastError)
            return new Tab(x['title'], x['url'], x['categoryName'], x?.['faviconUrl'], x?.[`dateAdded`]);
          });
          console.log(tabList);
          catList = data[`stoTabs`][1].map((x: any) => {
            return new Category(x.title, x.color, x.visible);
          });
          res([tabList, catList])
        })
      } catch (error) {
        console.log('data read failed')
        console.log(error)
       rej(error)
      }
    })
  }

  clearStorage = () => {
    chrome.storage.sync.remove(['stoTabs'])
  }

}

class Tab {
  constructor(title:string, url:string, categoryName:string, faviconUrl?:string, dateAdded?:string){ //get category when creating
    this.title = title
    this.url = url
    this.categoryName = categoryName
    this.dateAdded = dateAdded ? dateAdded : undefined
    this.faviconUrl = faviconUrl?faviconUrl : `assets/globe.png` // undefined //chrome.extension.getURL('favicon.ico')
  }
  title: string
  url:string
  dateAdded:string|undefined
  faviconUrl:string|undefined
  categoryName:string
}

class Category {
  constructor(title:string, color?:string, visible?:boolean){
    this.title = title
    this.color = color?color:``
    this.visible = visible?visible:false
  }
  title:string
  color:string
  visible:boolean
}

export {
  StorageService,
  Tab,
  Category
}
