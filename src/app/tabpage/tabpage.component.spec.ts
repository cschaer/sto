import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabpageComponent } from './tabpage.component';

describe('TabpageComponent', () => {
  let component: TabpageComponent;
  let fixture: ComponentFixture<TabpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabpageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TabpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
