import { formatDate } from '@angular/common';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Category, StorageService, Tab } from '../storage.service';

@Component({
  selector: 'app-tabpage',
  templateUrl: './tabpage.component.html',
  styleUrls: ['./tabpage.component.scss']
})
export class TabpageComponent implements OnInit {

  tabList!:Array<Tab>
  categoryList!:Array<Category>

  constructor(
    private storageService: StorageService,
    @Inject(LOCALE_ID) private locale:string
  ) {}

  ngOnInit(): void {
    this.updateLists().then(x=>{
      // console.log(this.categoryList[0])
      // console.log(this.filterByCategory(this.categoryList[0]))
    })
  }

  updateLists = async() => {
    await this.storageService.getFromStorage().then(x=>{
      this.tabList = [...x[0]]
      this.categoryList = [...x[1]]
    })
  }

  // refreshLists = async() => {
  //   this.tabList = [...this.tabList]
  //   this.categoryList = [...this.categoryList]
  // }

  filterByCategory = (category:Category):Tab[] => {
    // console.log(category); console.log(this.tabList[0])
    return this.tabList.filter(x => x.categoryName === category.title)
  }

  formatDate = (date:string|undefined):string|undefined => {
    // console.log(date)
    if (date === undefined)
      return undefined
    return formatDate(date, `dd-MMM-yy`, this.locale)
  }

}
