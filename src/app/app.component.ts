/// <reference types="chrome"/>
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(){
    this.whereAmI()
  }

  title:string = 'sto-extension';
  testText:string = ''
  isDropdown:boolean = false

  categories = [
    {id:'inspo',       title:'Inspo',       color: 'lightgreen'},
    {id:'garten',      title:'Garten',      color: 'green'},
    {id:'essen',       title:'Essen',       color: 'teal'},
    {id:'server-plex', title:'Server/Plex', color: 'orange'},
    {id:'travel',      title:'Travel',      color: 'purple'},
    {id:'hydro',       title:'Hydro',       color: 'blue'},
    {id:'project',     title:'Project',     color: 'red'},
    {id:'music',       title:'Music',       color: 'yellow'},
    {id:'german',      title:'German',      color: 'gold'},
    {id:'fallout-mods',title:'Fallout Mods',color: 'orange'},
  ]


  whereAmI = () => {
    console.debug(window.innerWidth)
    if (window.innerWidth < 200) {
      this.isDropdown = true
    }
  }

  getAndSave = async ( category:string ) => {
        // console.log(category)
        // let current = chrome.tabs.getCurrent

        chrome.tabs.query({active: true, currentWindow: true }, (tabs: chrome.tabs.Tab[]) =>{
            var currentUrl = tabs[0].url
            var tabId = tabs[0].id
            var pageTitle = tabs[0].title
            console.log(currentUrl)
            // document.getElementById("test").innerHTML=currentUrl
            this.testText = `${currentUrl}`
            // this.sendToApi(category, currentUrl, pageTitle, tabId)
          // this.sendToApi(category, window.location.href, document.title, tabId)
        })
  }

    sendToApi = (category: string, url: string|any, title: string|any, tabId: any) => {
      console.log(url)
      console.log(title)
      fetch(`http://localhost:3000/?category=${category}&title=${title}&url=${url}`)
        .then((res:any)=>{
          console.log(res)
            if (!res.ok)
              console.log(res)
                // document.getElementById("test").innerHTML = "failed"
            return res.text()
        }).then((res:string)=>{
            // document.getElementById("test").innerHTML = res
            this.testText = res
            chrome.tabs.remove(tabId)
            // window.close()
    })}

}
