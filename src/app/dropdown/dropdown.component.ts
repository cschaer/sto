/// <reference types="chrome"/>
import { Component, OnInit } from '@angular/core';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {

  constructor(
    private storageService: StorageService
  ){}

  ngOnInit(): void {
    // this.storageService.storeData()
    // this.storageService.getFromStorage()
  }

  title = 'sto-extension';
  testText = ''

  categories = [
    {id:'inspo',       title:'Inspo',       color: 'lightgreen'},
    {id:'garten',      title:'Garten',      color: 'green'},
    {id:'essen',       title:'Essen',       color: 'teal'},
    {id:'server-plex', title:'Server/Plex', color: 'orange'},
    {id:'travel',      title:'Travel',      color: 'purple'},
    {id:'hydro',       title:'Hydro',       color: 'blue'},
    {id:'project',     title:'Project',     color: 'red'},
    {id:'music',       title:'Music',       color: 'yellow'},
    {id:'german',      title:'German',      color: 'gold'},
    {id:'fallout-mods',title:'Fallout Mods',color: 'orange'},
  ]

  getAndSave = async ( categoryName:string ) => {
        console.debug(categoryName)
        chrome.tabs.query({ active: true, currentWindow: true }, (tabs: chrome.tabs.Tab[]) =>{
        let saveSuccess = this.storageService.saveTab(
          tabs[0].title, tabs[0].url, categoryName, tabs[0].favIconUrl, Date()
        ).then( x => {
          console.log(x)
          console.log(`save succ: ${x}`)
        })
          // if (saveSuccess) {
          //   // chrome.tabs.remove(tabs[0].id)
          // } else {}
          // // this.sendToApi(category, currentUrl, pageTitle, tabId)
        })
  }

    // sendToApi = (category: string, url: string|any, title: string|any, tabId: any) => {
    //   console.log(url)
    //   console.log(title)
    //   fetch(`http://localhost:3000/?category=${category}&title=${title}&url=${url}`)
    //     .then((res:any)=>{
    //       console.log(res)
    //         if (!res.ok)
    //           console.log(res)
    //             // document.getElementById("test").innerHTML = "failed"
    //         return res.text()
    //     }).then((res:string)=>{
    //         // document.getElementById("test").innerHTML = res
    //         this.testText = res
    //         chrome.tabs.remove(tabId)
    //         // window.close()
    // })}


}

